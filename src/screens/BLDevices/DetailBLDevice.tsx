import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../../routes/types';

type NavigationProp = StackNavigationProp<
  RootStackParamList,
  'DetailBLDeviceScreen'
>;
type Props = {
  navigation: NavigationProp;
};

const DetailBLDevice = (props: Props) => {
  const navigation = useNavigation<NavigationProp>();

  return (
    <View>
      <Text>DetailBLDevice</Text>
    </View>
  );
};

export {DetailBLDevice};

const styles = StyleSheet.create({});
