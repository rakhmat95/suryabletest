import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../../routes/types';
import {Button, Content, H2, List, Spinner, Text, View} from 'native-base';
import {useBLE} from '../../hooks';
import {Colors} from '../../constant';
import {Linking} from 'react-native';
import usePermission from '../../hooks/usePermission';
import {Peripheral} from 'react-native-ble-manager';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/reducers';
import RenderItem from '../../components/RenderItem';

type NavigationProp = StackNavigationProp<
  RootStackParamList,
  'SearchBLDeviceScreen'
>;
type Props = {
  navigation: NavigationProp;
};

const SearchBLDevice = (props: Props) => {
  const navigation = useNavigation<NavigationProp>();
  const discoverablePeripherals = useSelector(
    (rootState: RootState) => rootState.discoverablePeripherals,
  );
  const {hasPermission, checkPermission, requestPermission} = usePermission();
  const {
    isScanning,
    isConnected,
    startScan,
    stopScan,
    startNotification,
    stopNotification,
  } = useBLE();

  return (
    <Content style={{paddingVertical: 16, paddingHorizontal: 14}}>
      <H2
        style={{
          textAlign: 'center',
          color: Colors.PRIMARY_COLOR,
          fontWeight: '500',
        }}>
        BLE (Bluetooth Low Energy)
      </H2>
      <Text
        style={{
          fontSize: 18,
          textAlign: 'center',
          color: Colors.DEEP_DARK,
          fontWeight: '500',
        }}>
        {' '}
        by Rakhmat
      </Text>
      <View
        style={{
          marginVertical: 16,
        }}>
        {!hasPermission.bluetooth && (
          <Button
            full
            style={{marginBottom: 10, backgroundColor: Colors.PRIMARY_COLOR}}
            onPress={async () => {
              await requestPermission('bluetooth');
              const isGranted = await checkPermission();
              if (!isGranted) {
                await Linking.openSettings();
              }
            }}>
            <Text>Ask Permission BL</Text>
          </Button>
        )}
        {!isScanning ? (
          <Button
            full
            style={{marginBottom: 10, backgroundColor: Colors.PRIMARY_COLOR}}
            onPress={startScan}>
            <Text>Start Scan</Text>
          </Button>
        ) : (
          <Button
            full
            style={{marginBottom: 10, backgroundColor: Colors.RED}}
            onPress={stopScan}>
            <Text>Stop Scan</Text>
          </Button>
        )}

        {isScanning && <Spinner size={40} style={{marginVertical: 16}} />}

        <List>
          {Object.values(discoverablePeripherals).map((val: Peripheral) => {
            return <RenderItem key={val.id} val={val} />;
          })}
        </List>
      </View>
    </Content>
  );
};

export {SearchBLDevice};
