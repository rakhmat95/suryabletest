export const calculateDistance = (rssi: number) => {
  if (rssi === 0) {
    return -1.0;
  }

  const txPower = -59;
  const ratio = (rssi * 1.0) / txPower;

  if (ratio < 1.0) {
    return Math.pow(ratio, 10);
  } else {
    const distance = 0.89976 * Math.pow(ratio, 7.7095) + 0.111;
    return distance;
  }
};

export const bytesToString = (bytes: any) => {
  const decoder = new TextDecoder();
  const text = decoder.decode(bytes);
  return text;
};
