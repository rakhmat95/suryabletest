import React from 'react';
import {Root} from 'native-base';
import Routes from './routes';
import {SafeAreaProvider} from 'react-native-safe-area-context';
// import FlashMessage from 'react-native-flash-message';

const Boot = () => {
  return (
    <SafeAreaProvider>
      {/* <FlashMessage position="top" /> */}
      <Root>
        <Routes />
      </Root>
    </SafeAreaProvider>
  );
};

export default Boot;
