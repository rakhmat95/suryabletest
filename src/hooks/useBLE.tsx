import {useEffect, useState} from 'react';
import {NativeEventEmitter, NativeModules, Platform} from 'react-native';
import BleManager, {
  BleScanCallbackType,
  BleScanMatchMode,
  BleScanMode,
  BleState,
  Peripheral,
} from 'react-native-ble-manager';
import BLECustomModule from '../modules/BLECustomModule';
import usePermission from './usePermission';
import {Toast} from 'native-base';
import {useDispatch} from 'react-redux';
import {
  addPeripheralAction,
  clearPeripheralAction,
} from '../redux/actions/DiscoverablePeripheralsAction';
import {bytesToString} from '../utils/helpers';

type Props = {};

const SECONDS_TO_SCAN_FOR = 5;
const SERVICE_UUIDS: string[] = [];
const ALLOW_DUPLICATES = false;

const BleManagerModule = NativeModules.BleManager;
const BleIOSModule = NativeModules.BLEModule;

const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const bleIOSEmitter = new NativeEventEmitter(BleIOSModule);

const useBLE = (props?: Props) => {
  const dispatch = useDispatch();

  const {hasPermission, requestPermission} = usePermission();
  const [hasStarted, setHasStarted] = useState(false);
  const [isScanning, setIsScanning] = useState(false);
  const [isConnected, setIsConnected] = useState(false);

  const _enableBluetooth = async () => {
    try {
      console.debug('[enableBluetooth]');
      await BleManager.enableBluetooth();
    } catch (error) {
      console.error('[enableBluetooth] thrown', error);
    }
  };

  const _startService = async () => {
    if (!hasPermission.bluetooth) await requestPermission('bluetooth');

    if (Platform.OS == 'android') {
      BleManager.start({showAlert: false}).then(() => {
        console.log('Module initialized');
        setHasStarted(true);
      });
    }
  };

  const startScan = async () => {
    console.debug('[startScan]');

    dispatch(clearPeripheralAction());

    if (Platform.OS == 'android') {
      if (!hasPermission.bluetooth) {
        Toast.show({text: 'Need bluetooth permission.'});
        return;
      }
      if ((await BleManager.checkState()) == BleState.Off) {
        _enableBluetooth();
        Toast.show({text: 'Bluetooth need turned on.'});
        return;
      }
    } else if (Platform.OS == 'ios') {
      if (!(await BLECustomModule.isBluetoothPermitted())) {
        Toast.show({text: 'Need bluetooth permission.'});
        return;
      }
      // if (!(await BLECustomModule.isBluetoothEnabled())) {
      //   Toast.show({text: 'Bluetooth need turned on.'});
      //   return;
      // }
    }

    if (!hasStarted) await _startService();

    if (!isScanning) {
      try {
        setIsScanning(true);
        if (Platform.OS == 'android') {
          BleManager.scan(
            SERVICE_UUIDS,
            SECONDS_TO_SCAN_FOR,
            ALLOW_DUPLICATES,
            {
              matchMode: BleScanMatchMode.Sticky,
              scanMode: BleScanMode.LowLatency,
              callbackType: BleScanCallbackType.AllMatches,
            },
          ).then(() => {
            console.debug('[startScan] starting scan...');
            Toast.show({text: 'Start scanning.'});
          });
        } else {
          BLECustomModule.startScan().then(() => {
            console.debug('[startScan] starting scan...');
            setIsScanning(true);
          });
        }
      } catch (error) {
        console.error('[startScan] ble scan error thrown', error);
      }
    }
  };

  const stopScan = () => {
    setIsScanning(false);
    if (Platform.OS == 'android') {
      BleManager.stopScan().then(() => {
        Toast.show({text: 'Stop scanning.'});
        console.log('Scan stopped');
      });
    } else if (Platform.OS == 'ios') {
      BLECustomModule.stopScan().then(() => {
        Toast.show({text: 'Stop scanning.'});
        console.log('Scan stopped');
      });
    }
  };

  const startNotification = async (peripheralId: string) => {
    if (Platform.OS == 'android') {
      await BleManager.connect(peripheralId);
    } else if (Platform.OS == 'ios') {
      BLECustomModule.startNotification(peripheralId).then(() => {
        setIsConnected(true);
        Toast.show({text: 'Peripheral connected.'});
        console.log('Peripheral connected');
      });
    }
  };

  const stopNotification = async (peripheralId: string) => {
    if (Platform.OS == 'android') {
      // await BleManager.stopNotification(peripheralId, uuid, characteristic);
      BleManager.disconnect(peripheralId).then(() => {
        setIsConnected(false);
        Toast.show({text: 'Stop connected.'});
        console.log('Stop connected');
      });
    } else if (Platform.OS == 'ios') {
      BLECustomModule.stopNotification().then(() => {
        setIsConnected(false);
        Toast.show({text: 'Stop connected.'});
        console.log('Stop connected');
      });
    }
  };

  const handleDiscoverPeripheral = (peripheral: Peripheral) => {
    // console.debug('[handleDiscoverPeripheral] new BLE peripheral=', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'N/A';
    }

    dispatch(addPeripheralAction(peripheral));
  };

  const handleStopScan = () => {
    setIsScanning(false);
    console.debug('[handleStopScan] scan is stopped.');
  };

  const handleCharacteristicPeripheral = ({
    value,
    peripheral,
    characteristic,
    service,
  }: any) => {
    const data = bytesToString(value);
    console.log(`Received ${data} for characteristic ${characteristic}`);

    BleManager.retrieveServices(peripheral);
    BleManager.startNotification(peripheral, peripheral, characteristic).then(
      () => {
        setIsConnected(true);
        Toast.show({text: 'Peripheral connected.'});
        console.log('Peripheral connected');
      },
    );
  };

  useEffect(() => {
    _startService();

    if (Platform.OS === 'android') {
      const listeners: any = [
        bleManagerEmitter.addListener(
          'BleManagerDiscoverPeripheral',
          handleDiscoverPeripheral,
        ),
        bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan),
        bleManagerEmitter.addListener(
          'BleManagerDidUpdateValueForCharacteristic',
          handleCharacteristicPeripheral,
        ),
      ];

      return () => {
        for (const listener of listeners) {
          listener.remove();
        }
      };
    } else if (Platform.OS === 'ios') {
      const listeners: any = [
        bleIOSEmitter.addListener(
          'BleManagerDiscoverPeripheral',
          handleDiscoverPeripheral,
        ),
        bleIOSEmitter.addListener('BleManagerStopScan', handleStopScan),
        bleIOSEmitter.addListener(
          'BleManagerDidUpdateValueForCharacteristic',
          handleCharacteristicPeripheral,
        ),
      ];

      return () => {
        for (const listener of listeners) {
          listener.remove();
        }
      };
    }
  }, []);

  return {
    hasPermission,
    isScanning,
    isConnected,
    startScan,
    stopScan,
    startNotification,
    stopNotification,
  };
};

export default useBLE;
