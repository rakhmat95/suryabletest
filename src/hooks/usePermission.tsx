import {Toast} from 'native-base';
import {useEffect, useState} from 'react';
import {AppState, PermissionsAndroid, Platform} from 'react-native';
import BLECustomModule from '../modules/BLECustomModule';

type Props = {};

type TypePermissionProps = 'bluetooth' | '';

const usePermission = (props?: Props) => {
  const [hasPermission, setHasPermission] = useState({
    bluetooth: false,
  });

  const requestPermission = async (type: TypePermissionProps) => {
    try {
      if (type == 'bluetooth') {
        if (Platform.OS == 'android') {
          await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
            PermissionsAndroid.PERMISSIONS.BLUETOOTH_ADVERTISE,
            PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
          ]);
        } else if (Platform.OS == 'ios') {
          await BLECustomModule.requestBluetoothPermission();
        }

        checkPermission();
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const checkPermission = async () => {
    // console.log('[checkPermission]');

    let blGranted = false;

    if (Platform.OS == 'android') {
      blGranted = await PermissionsAndroid.check(
        'android.permission.BLUETOOTH_SCAN',
      );
    } else if (Platform.OS == 'ios') {
      blGranted = await BLECustomModule.isBluetoothPermitted();
    }

    if (!blGranted) Toast.show({text: 'Bluetooth permission denied.'});

    setHasPermission((prev: any) => ({
      ...prev,
      bluetooth: blGranted,
    }));

    return blGranted;
  };

  const handleAppStateChange = async (nextAppState: any) => {
    if (
      !hasPermission.bluetooth &&
      AppState.currentState.match(/active/) &&
      nextAppState === 'active'
    ) {
      await checkPermission();
    }
  };

  useEffect(() => {
    checkPermission();
  }, []);

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);
  }, []);

  return {hasPermission, checkPermission, requestPermission};
};

export default usePermission;
