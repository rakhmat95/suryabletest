export type RootStackParamList = {
  SearchBLDeviceScreen: undefined;
  DetailBLDeviceScreen: {id: string} | undefined;
};
