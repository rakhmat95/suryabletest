import {createStackNavigator} from '@react-navigation/stack';
import {DetailBLDevice, SearchBLDevice} from '../screens';
import {RootStackParamList} from './types';

const MainStack = createStackNavigator<RootStackParamList>();
const MainStackScreen = (props: any) => {
  return (
    <MainStack.Navigator
      initialRouteName="SearchBLDeviceScreen"
      screenOptions={{
        headerShown: false,
        animationEnabled: true,
        cardOverlayEnabled: true,
        cardShadowEnabled: true,
      }}>
      <MainStack.Screen
        name="SearchBLDeviceScreen"
        component={SearchBLDevice}
      />
      <MainStack.Screen
        name="DetailBLDeviceScreen"
        component={DetailBLDevice}
      />
    </MainStack.Navigator>
  );
};

export default MainStackScreen;
