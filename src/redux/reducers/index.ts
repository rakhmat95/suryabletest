import {combineReducers} from 'redux';

import {discoverablePeripheralsReducer} from './DiscoverablePeripheralsReducer';

export const rootReducer = combineReducers({
  discoverablePeripherals: discoverablePeripheralsReducer,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
