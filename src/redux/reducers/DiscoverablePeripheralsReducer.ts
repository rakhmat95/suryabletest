import discoverablePeripheralSlice from '../slices/DiscoverablePeripheralsSlice';

export const discoverablePeripheralsReducer =
  discoverablePeripheralSlice.reducer;
