import {Peripheral} from 'react-native-ble-manager';
import discoverablePeripheralSlice from '../slices/DiscoverablePeripheralsSlice';

const addPeripheralAction = (payload: Peripheral) => {
  return discoverablePeripheralSlice.actions.addPeripheral(payload);
};

const clearPeripheralAction = () => {
  return discoverablePeripheralSlice.actions.clearPeripherals();
};

export {addPeripheralAction, clearPeripheralAction};
