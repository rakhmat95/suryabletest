import {PayloadAction, createSlice} from '@reduxjs/toolkit';
import {Peripheral} from 'react-native-ble-manager';

interface PeripheralState {
  [id: string]: Peripheral;
}

const initialState: PeripheralState = {};

const discoverablePeripheralSlice = createSlice({
  name: 'peripherals',
  initialState: initialState,
  reducers: {
    addPeripheral(state, action: PayloadAction<Peripheral>) {
      state[action.payload.id] = action.payload;
    },
    clearPeripherals() {
      return initialState;
    },
  },
});

export default discoverablePeripheralSlice;
