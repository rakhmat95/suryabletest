import {NativeEventEmitter, NativeModules} from 'react-native';

const bleIOSModule = NativeModules.BLEModule;

/**
 * Custom native module BLE IOS, based on the task
 */
class BLECustomModule extends NativeEventEmitter {
  constructor() {
    super(bleIOSModule);
  }

  startScan() {
    return new Promise<void>((fulfill, reject) => {
      bleIOSModule.startScan((data: any) => {
        fulfill(data);
      });
    });
  }

  stopScan() {
    return new Promise<void>((fulfill, reject) => {
      bleIOSModule.stopScan((data: any) => {
        fulfill(data);
      });
    });
  }

  isBluetoothEnabled(): Promise<boolean> {
    return new Promise<boolean>((fulfill, reject) => {
      bleIOSModule.isBluetoothEnabled((data: any) => {
        fulfill(data);
      });
    });
  }

  isBluetoothPermitted(): Promise<boolean> {
    return new Promise<boolean>((fulfill, reject) => {
      bleIOSModule.isBluetoothPermitted((data: any) => {
        fulfill(data);
      });
    });
  }

  requestBluetoothPermission() {
    return new Promise<void>((fulfill, reject) => {
      bleIOSModule.requestBluetoothPermission((data: any) => {
        fulfill(data);
      });
    });
  }

  startNotification(peripheralId: string) {
    return new Promise<void>((fulfill, reject) => {
      bleIOSModule.startNotification(peripheralId, (error: any) => {
        if (error) {
          console.error(error)
          reject(error);
        } else {
          fulfill();
        }
      });
    });
  }

  stopNotification() {
    return new Promise<void>((fulfill, reject) => {
      bleIOSModule.stopNotification((error: string | null) => {
        if (error) {
          console.error(error)
          reject(error);
        } else {
          fulfill();
        }
      });
    });
  }
}

export default new BLECustomModule();
