import React, {Key, useState} from 'react';
import {Button, Text, View} from 'native-base';
import {Colors} from '../constant';
import {useBLE} from '../hooks';
import {Peripheral} from 'react-native-ble-manager';
import {calculateDistance} from '../utils/helpers';

type Props = {
  val: Peripheral;
};

const RenderItem = (props: Props) => {
  const {val} = props;
  const {startNotification, stopNotification} = useBLE();

  const [isConnected, setIsConnected] = useState(false);

  const toggleConnect = async (id: string) => {
    if (!isConnected) {
      startNotification(id).then(() => {
        setIsConnected(true);
      });
    } else {
      stopNotification(id).then(() => {
        setIsConnected(false);
      });
    }
  };

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 16,
        borderBottomColor: Colors.DEEP_DARK,
        borderBottomWidth: 1,
        paddingBottom: 16,
      }}>
      <View>
        <Text>id: {val.id}</Text>
        <Text>name: {val.name}</Text>
        <Text>Appx. Distance: {calculateDistance(val.rssi).toFixed(2)} m</Text>
        {val.advertising.serviceUUIDs != null && (
          <>
            <Text>uuid: {val.advertising.serviceUUIDs[0]}</Text>
            <Text>characteristic uuid: {val.advertising.serviceUUIDs[0]}</Text>
          </>
        )}
      </View>
      <View style={{justifyContent: 'center'}}>
        {val.advertising.isConnectable && (
          <Button
            style={{
              marginBottom: 10,
              backgroundColor: !isConnected ? Colors.GREEN : Colors.RED,
            }}
            onPress={() => {
              toggleConnect(val.id);
            }}>
            <Text>{!isConnected ? 'Start Notif' : 'Stop Notif'}</Text>
          </Button>
        )}
      </View>
    </View>
  );
};

export default RenderItem;
