export const Colors = {
  PRIMARY_COLOR: '#06b6d4',
  DEEP_DARK: '#30334F',
  RED: '#F66785',
  GREEN: '#A1CC7A',
};
