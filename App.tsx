import React, {useRef} from 'react';
import {Provider} from 'react-redux';
import {getStore, getPersistor} from './src/redux/store';
import {NavigationContainer} from '@react-navigation/native';
import {PersistGate} from 'redux-persist/integration/react';
import Root from './src';
import {enableMapSet} from 'immer';

enableMapSet();

const store = getStore();
const persistor = getPersistor();

function App(): React.JSX.Element {
  const navigationRef = useRef<any>();
  const routeNameRef = useRef();

  return (
    <Provider store={store}>
      <NavigationContainer
        ref={navigationRef}
        onReady={() => {
          routeNameRef.current =
            navigationRef?.current?.getCurrentRoute()?.name;
        }}
        onStateChange={async () => {
          const previousRouteName = routeNameRef?.current;
          const currentRouteName =
            navigationRef?.current?.getCurrentRoute()?.name;
          if (previousRouteName !== currentRouteName) {
            routeNameRef.current = currentRouteName;
          }
        }}>
        <PersistGate loading={null} persistor={persistor}>
          <Root />
        </PersistGate>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
