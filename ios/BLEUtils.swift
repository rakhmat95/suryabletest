import Foundation
import CoreBluetooth

class BLEUtils {
  static func decodeCharacteristicProperties(_ p: CBCharacteristicProperties) -> Dictionary<String, Any> {
      var props: [String: Any] = [:]
      
      // NOTE: props strings need to be consistent across iOS and Android
      if p.contains(.broadcast) {
          props["Broadcast"] = "Broadcast"
      }
      
      if p.contains(.read) {
          props["Read"] = "Read"
      }
      
      if p.contains(.writeWithoutResponse) {
          props["WriteWithoutResponse"] = "WriteWithoutResponse"
      }
      
      if p.contains(.write) {
          props["Write"] = "Write"
      }
      
      if p.contains(.notify) {
          props["Notify"] = "Notify"
      }
      
      if p.contains(.indicate) {
          props["Indicate"] = "Indicate"
      }
      
      if p.contains(.authenticatedSignedWrites) {
          props["AuthenticateSignedWrites"] = "AuthenticateSignedWrites"
      }
      
      if p.contains(.extendedProperties) {
          props["ExtendedProperties"] = "ExtendedProperties"
      }
      
      if p.contains(.notifyEncryptionRequired) {
          props["NotifyEncryptionRequired"] = "NotifyEncryptionRequired"
      }
      
      if p.contains(.indicateEncryptionRequired) {
          props["IndicateEncryptionRequired"] = "IndicateEncryptionRequired"
      }
      
      return props
  }
  
  static func dataToArrayBuffer(_ data: Data) -> [String: Any] {
    return [
      "CDVType": "ArrayBuffer",
      "data": data.base64EncodedString(options: []),
      "bytes": data.map { $0 }
    ]
  }
  
  static func reformatAdvertisementData(_ advertisementData: [String:Any]) -> [String:Any] {
    var adv = advertisementData
    // Rename 'local name' key
    if let localName = adv[CBAdvertisementDataLocalNameKey] {
      adv.removeValue(forKey: CBAdvertisementDataLocalNameKey)
      adv["localName"] = localName
    }
    
    // Rename 'isConnectable' key
    if let isConnectable = adv[CBAdvertisementDataIsConnectable] {
      adv.removeValue(forKey: CBAdvertisementDataIsConnectable)
      adv["isConnectable"] = isConnectable
    }
    
    // Rename 'power level' key
    if let powerLevel = adv[CBAdvertisementDataTxPowerLevelKey] {
      adv.removeValue(forKey: CBAdvertisementDataTxPowerLevelKey)
      adv["txPowerLevel"] = powerLevel
    }
    
    // Service Data is a dictionary of CBUUID and NSData
    // Convert to String keys with Array Buffer values
    if let serviceData = adv[CBAdvertisementDataServiceDataKey] as? NSMutableDictionary {
      for (key, value) in serviceData {
        if let uuidKey = key as? CBUUID, let dataValue = value as? Data {
          serviceData.removeObject(forKey: uuidKey)
          serviceData[uuidKey.uuidString.lowercased()] = BLEUtils.dataToArrayBuffer(dataValue)
        }
      }
      
      adv.removeValue(forKey: CBAdvertisementDataServiceDataKey)
      adv["serviceData"] = serviceData
    }
    
    // Create a new list of Service UUIDs as Strings instead of CBUUIDs
    if let serviceUUIDs = adv[CBAdvertisementDataServiceUUIDsKey] as? NSMutableArray {
      var serviceUUIDStrings:Array<String> = []
      for value in serviceUUIDs {
        if let uuid = value as? CBUUID {
          serviceUUIDStrings.append(uuid.uuidString.lowercased())
        }
      }
      
      adv.removeValue(forKey: CBAdvertisementDataServiceUUIDsKey)
      adv["serviceUUIDs"] = serviceUUIDStrings
    }
    // Solicited Services UUIDs is an array of CBUUIDs, convert into Strings
    if let solicitiedServiceUUIDs = adv[CBAdvertisementDataSolicitedServiceUUIDsKey] as? NSMutableArray {
      var solicitiedServiceUUIDStrings:Array<String> = []
      for value in solicitiedServiceUUIDs {
        if let uuid = value as? CBUUID {
          solicitiedServiceUUIDStrings.append(uuid.uuidString.lowercased())
        }
      }
      
      adv.removeValue(forKey: CBAdvertisementDataSolicitedServiceUUIDsKey)
      adv["solicitedServiceUUIDs"] = solicitiedServiceUUIDStrings
    }
    
    // Overflow Services UUIDs is an array of CBUUIDs, convert into Strings
    if let overflowServiceUUIDs = adv[CBAdvertisementDataOverflowServiceUUIDsKey] as? NSMutableArray {
      var overflowServiceUUIDStrings:Array<String> = []
      for value in overflowServiceUUIDs {
        if let uuid = value as? CBUUID {
          overflowServiceUUIDStrings.append(uuid.uuidString.lowercased())
        }
      }
      
      adv.removeValue(forKey: CBAdvertisementDataOverflowServiceUUIDsKey)
      adv["overflowServiceUUIDs"] = overflowServiceUUIDStrings
    }
    
    // Convert the manufacturer data
    if let mfgData = adv[CBAdvertisementDataManufacturerDataKey] as? Data {
      if mfgData.count > 1 {
        let manufactureID = UInt16(mfgData[0]) + UInt16(mfgData[1]) << 8
        var manInfo: [String: Any] = [:]
        manInfo[String(format: "%04x", manufactureID)] = BLEUtils.dataToArrayBuffer(mfgData.subdata(in: 2..<mfgData.endIndex))
        adv["manufacturerData"] = manInfo
      }
      adv.removeValue(forKey: CBAdvertisementDataManufacturerDataKey)
      adv["manufacturerRawData"] = BLEUtils.dataToArrayBuffer(mfgData)
      
    }
    
    return adv
  }
  
  static func findCharacteristic(fromUUID UUID: CBUUID, service: CBService, prop: CBCharacteristicProperties) -> CBCharacteristic? {
      for characteristic in service.characteristics ?? [] {
          if (characteristic.properties.contains(prop) && characteristic.uuid.isEqual(UUID)) {
              return characteristic
          }
      }
      return nil // Characteristic with prop not found on this service
  }
  
  static func findCharacteristic(fromUUID UUID: CBUUID, service: CBService) -> CBCharacteristic? {
      for characteristic in service.characteristics ?? [] {
          if characteristic.uuid.isEqual(UUID) {
              return characteristic
          }
      }
      return nil // Characteristic not found on this service
  }
}
