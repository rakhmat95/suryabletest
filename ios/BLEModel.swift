import Foundation
import CoreBluetooth

class Peripheral:Hashable {
    var instance: CBPeripheral
    var rssi: NSNumber?
    var advertisementData: [String:Any]?
    
    init(peripheral: CBPeripheral, rssi: NSNumber? = nil, advertisementData: [String:Any]? = nil) {
        self.instance = peripheral
        self.rssi = rssi
        self.advertisementData = advertisementData
    }
    
    func setRSSI(_ newRSSI: NSNumber?) {
        self.rssi = newRSSI
    }
    
    func setAdvertisementData(_ advertisementData: [String:Any]) {
        self.advertisementData = advertisementData
    }
    
    func advertisingInfo() -> Dictionary<String, Any> {
        var peripheralInfo: [String: Any] = [:]
        
        peripheralInfo["name"] = instance.name
        peripheralInfo["id"] = instance.uuidAsString()
        peripheralInfo["rssi"] = self.rssi
        if let adv = self.advertisementData {
            peripheralInfo["advertising"] = BLEUtils.reformatAdvertisementData(adv)
        }
        
        return peripheralInfo
    }
    
    func servicesInfo() -> Dictionary<String, Any> {
        var servicesInfo: [String: Any] = advertisingInfo()
        
        var serviceList = [[String: Any]]()
        var characteristicList = [[String: Any]]()
        
        for service in instance.services ?? [] {
            var serviceDictionary = [String: Any]()
            serviceDictionary["uuid"] = service.uuid.uuidString.lowercased()
            serviceList.append(serviceDictionary)
            
            for characteristic in service.characteristics ?? [] {
                var characteristicDictionary = [String: Any]()
                characteristicDictionary["service"] = service.uuid.uuidString.lowercased()
                characteristicDictionary["characteristic"] = characteristic.uuid.uuidString.lowercased()
                
                if let value = characteristic.value, value.count > 0 {
                    characteristicDictionary["value"] = BLEUtils.dataToArrayBuffer(value)
                }
                
                characteristicDictionary["properties"] = BLEUtils.decodeCharacteristicProperties(characteristic.properties)
                
                characteristicDictionary["isNotifying"] = characteristic.isNotifying
                
                var descriptorList = [[String: Any]]()
                for descriptor in characteristic.descriptors ?? [] {
                    var descriptorDictionary = [String: Any]()
                    descriptorDictionary["uuid"] = descriptor.uuid.uuidString.lowercased()
                    
                    if let value = descriptor.value {
                        descriptorDictionary["value"] = value
                    }
                    
                    descriptorList.append(descriptorDictionary)
                }
                
                if descriptorList.count > 0 {
                    characteristicDictionary["descriptors"] = descriptorList
                }
                
                characteristicList.append(characteristicDictionary)
            }
        }
        
        servicesInfo["services"] = serviceList
        servicesInfo["characteristics"] = characteristicList
        
        return servicesInfo
    }
    
    
    static func == (lhs: Peripheral, rhs: Peripheral) -> Bool {
        return lhs.instance.uuidAsString() == rhs.instance.uuidAsString()
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(instance.uuidAsString())
    }
}

extension CBPeripheral {
    
    func uuidAsString() -> String {
        return self.identifier.uuidString.lowercased()
    }
}
