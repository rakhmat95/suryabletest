#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventDispatcher.h"

@interface RCT_EXTERN_MODULE(BLEModule, NSObject)

RCT_EXTERN_METHOD(startScan:
                  (nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(stopScan:
                  (nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(isBluetoothEnabled:
                  (nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(isBluetoothPermitted:
                  (nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(requestBluetoothPermission:
                  (nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(startNotification:
                  (NSString *)peripheralUUID
                  callback:(nonnull RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(stopNotification:
                  (nonnull RCTResponseSenderBlock)callback)

@end
