import Foundation
import CoreBluetooth
import CoreLocation

@objc(BLEModule)
class BLEModule: RCTEventEmitter, CBCentralManagerDelegate, CBPeripheralDelegate, CLLocationManagerDelegate {
  
    private var centralManager: CBCentralManager
    private var locationManager: CLLocationManager
    private var hasListeners:Bool = false
    private var peripherals: Dictionary<String, Peripheral> = [:]
    private var connectedPeripheral: Peripheral?
    private var currentCharacteristic: CBCharacteristic?

    private override init() {
        centralManager = CBCentralManager(delegate: nil, queue: nil)
        locationManager = CLLocationManager()
        super.init()

        centralManager.delegate = self
        locationManager.delegate = self
    }
  
    @objc override static func requiresMainQueueSetup() -> Bool { return true }

    @objc override static func moduleName() -> String {
        return "BLEModule"
    }
  
    @objc override func supportedEvents() -> [String]! {
      return ["BleManagerDiscoverPeripheral", "BleManagerStartScan", "BleManagerStopScan", "BleManagerConnectPeripheral", "BleManagerDisconnectPeripheral", "BleManagerStartNotification", "BleManagerStopNotification", "BleManagerDidUpdateNotificationStateFor", "BleManagerDidUpdateValueForCharacteristic"]
    }
  
    @objc override func startObserving() {
        hasListeners = true
    }
    
    @objc override func stopObserving() {
        hasListeners = false
    }

    @objc public func startScan(_ callback: @escaping RCTResponseSenderBlock) {
        NSLog("[startScan] called")

        peripherals.removeAll()
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        sendEvent(withName: "BleManagerStartScan", body: nil)
      
        callback(["[startScan] called"])
    }

    @objc public func stopScan(_ callback: @escaping RCTResponseSenderBlock) {
        NSLog("[stopScan] called")

        centralManager.stopScan()
        sendEvent(withName: "BleManagerStopScan", body: nil)
      
        callback(["[stopScan] called"])
    }
  
    @objc public func isBluetoothEnabled(_ callback: @escaping RCTResponseSenderBlock) {
        NSLog("[isBluetoothEnabled] called")
      
        callback([centralManager.state == .poweredOn])
    }

    @objc public func isBluetoothPermitted(_ callback: @escaping RCTResponseSenderBlock) {
      NSLog("[isBluetoothPermitted] called")
      
      callback([CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse])
    }

    @objc public func requestBluetoothPermission(_ callback: @escaping RCTResponseSenderBlock) {
        NSLog("[requestBluetoothPermission] called")
      
      if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
          callback([true])
      } else {
          locationManager.requestWhenInUseAuthorization()
          callback([false])
      }
    }
  
    @objc public func startNotification(_ peripheralUUID: String,
                                 callback: @escaping RCTResponseSenderBlock) {
      NSLog("[startNotification] called")
      
      if let peripheral = peripherals[peripheralUUID] {
        if connectedPeripheral != nil {
          centralManager.cancelPeripheralConnection(connectedPeripheral!.instance)
          currentCharacteristic = nil
          
          if (hasListeners) {
              sendEvent(withName: "BleManagerDisconnectPeripheral", body: nil)
          }
        }
        centralManager.connect(peripheral.instance, options: nil)
        connectedPeripheral = peripheral

        if (hasListeners) {
            sendEvent(withName: "BleManagerConnectPeripheral", body: nil)
        }
      } else {
        callback(["Peripheral not exist."])
      }
    }
  
    @objc public func stopNotification(_ callback: @escaping RCTResponseSenderBlock) {
        NSLog("[stopNotification] called")

        if connectedPeripheral != nil && currentCharacteristic != nil {
          connectedPeripheral!.instance.setNotifyValue(false, for: currentCharacteristic!)
        }
      
        callback(["No connected peripherals."])
    }
  
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let services = peripheral.services {
            for service in services {
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let characteristics = service.characteristics {
            for characteristic in characteristics {
                peripheral.discoverDescriptors(for: characteristic)
            }
        }
    }
  
    func peripheral(_ peripheral: CBPeripheral,
                  didUpdateValueFor characteristic: CBCharacteristic,
                  error: Error?) {
        if connectedPeripheral != nil {
            currentCharacteristic = characteristic
            sendEvent(withName: "BleManagerDidUpdateValueForCharacteristic", body: [
              "peripheral": peripheral.uuidAsString(),
              "characteristic": characteristic.uuid.uuidString.lowercased(),
              "service": characteristic.service!.uuid.uuidString.lowercased(),
              "value": characteristic.value
            ])
            peripheral.setNotifyValue(true, for: characteristic)
            
            if (hasListeners) {
              sendEvent(withName: "BleManagerStartNotification", body: nil)
            }
        }
    }
  
    func peripheral(_ peripheral: CBPeripheral,
                  didUpdateNotificationStateFor characteristic: CBCharacteristic,
                  error: Error?) {
    
        if let error = error {
            NSLog("Error in didUpdateNotificationStateForCharacteristic: \(error)")
            
            if hasListeners {
                sendEvent(withName: "BleManagerDidUpdateNotificationStateFor", body: [
                    "peripheral": peripheral.uuidAsString(),
                    "characteristic": characteristic.uuid.uuidString.lowercased(),
                    "isNotifying": false,
                    "domain": error._domain,
                    "code": error._code
                ])
            }
        } else {
            if hasListeners {
                sendEvent(withName: "BleManagerDidUpdateNotificationStateFor", body: [
                    "peripheral": peripheral.uuidAsString(),
                    "characteristic": characteristic.uuid.uuidString.lowercased(),
                    "isNotifying": characteristic.isNotifying
                ])
            }
        }
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("centralManagerDidUpdateState: unknown")
        case .resetting:
            print("centralManagerDidUpdateState: resetting")
        case .unsupported:
            print("centralManagerDidUpdateState: unsupported")
        case .unauthorized:
            print("centralManagerDidUpdateState: unauthorized")
        case .poweredOff:
            print("centralManagerDidUpdateState: poweredOff")
        case .poweredOn:
            print("centralManagerDidUpdateState: poweredOn")
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi: NSNumber) {
      var cp: Peripheral? = nil
      if let p = peripherals[peripheral.uuidAsString()] {
          cp = p
          cp?.setRSSI(rssi)
          cp?.setAdvertisementData(advertisementData)
      } else {
          cp = Peripheral(peripheral:peripheral, rssi:rssi, advertisementData:advertisementData)
          peripherals[peripheral.uuidAsString()] = cp
      }
      
      if (hasListeners) {
          sendEvent(withName: "BleManagerDiscoverPeripheral", body: cp?.advertisingInfo())
      }
    }
}
