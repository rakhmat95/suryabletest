# React Native BLE Module for iOS

This React Native module manages the functionality of searching for Bluetooth Low Energy (BLE) peripherals on iOS. It allows starting and stopping BLE scans, retrieving scan results, and handling notifications for significant events.

## Features

1. **Initiate and terminate scanning for nearby BLE peripherals.**
2. **Retrieve scan results with details about each discovered peripheral:**
   - Unique identifier
   - Name
   - Signal strength (RSSI)
   - Advertisement data
3. **Notifications for significant events:**
   - Scanning starts
   - Scanning ends

## Requirements

- iOS development with Swift
- React Native development with TypeScript

## Installation

1. **Install the necessary packages:**

   ```bash
   npm install
   cd ios
   pod install

2. **Communication with the native module:**

   ```typescript
   import {NativeModules} from 'react-native';
   import BLECustomModule from '../modules/BLECustomModule';
   ...

   const BleIOSModule = NativeModules.BLEModule;
   const bleIOSEmitter = new NativeEventEmitter(BleIOSModule);

   const useBLE = (props: Props) => {
      ...
      
      BLECustomModule.startScan()
      BLECustomModule.stopScan()
    
      BLECustomModule.startNotification(peripheralId)  
      BLECustomModule.stopNotification(peripheralId)  
      
      const handleDiscoverPeripheral = (peripheral: Peripheral) => {
        // write your code here
      };
      
      useEffect(() => {
          const listeners: any = [
            bleIOSEmitter.addListener(
              'BleManagerDiscoverPeripheral',
              handleDiscoverPeripheral,
            ),
            bleIOSEmitter.addListener('BleManagerStopScan', handleStopScan),
            bleIOSEmitter.addListener(
              'BleManagerDidUpdateValueForCharacteristic',
              handleCharacteristicPeripheral,
            ),
          ];
    
          return () => {
            for (const listener of listeners) {
              listener.remove();
            }
          };
      }, [])
  
      ...
   }
   
3. **Example usage:**

   ```bash
   import {useBLE} from '../../hooks';

   const SearchBLDevice = (props: Props) => {
      ...
      const {
        startScan,
        stopScan,
        startNotification,
        stopNotification,
      } = useBLE();
      ...
   }